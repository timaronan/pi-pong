-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 08, 2015 at 06:15 AM
-- Server version: 5.5.36
-- PHP Version: 5.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pong`
--

-- --------------------------------------------------------

--
-- Table structure for table `games`
--

CREATE TABLE IF NOT EXISTS `games` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serving` int(1) NOT NULL,
  `setz` int(11) NOT NULL,
  `winner` int(1) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `body_shot` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2110 ;

-- --------------------------------------------------------

--
-- Table structure for table `matchz`
--

CREATE TABLE IF NOT EXISTS `matchz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p1` int(11) NOT NULL,
  `p2` int(11) NOT NULL,
  `p3` int(11) NOT NULL,
  `p4` int(11) NOT NULL,
  `p1r` int(11) NOT NULL,
  `p2r` int(11) NOT NULL,
  `p3r` int(11) NOT NULL,
  `p4r` int(11) NOT NULL,
  `sets` int(2) NOT NULL,
  `games` int(2) NOT NULL,
  `initiated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=227 ;

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE IF NOT EXISTS `players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `nickname` varchar(100) NOT NULL,
  `ronan_rating` int(3) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `players`
--

INSERT INTO `players` (`id`, `name`, `nickname`, `ronan_rating`, `date_added`) VALUES
(1, 'Brian Beltz', 'Beltz', 25, '2015-06-02 21:28:10'),
(2, 'Kiernan Hopkins', 'Sports Star', 25, '2015-06-02 21:29:26'),
(3, 'Ryan Asignacion', 'SLAM!', 25, '2015-06-02 21:32:30'),
(4, 'David Perez', 'si, es guapo', 25, '2015-06-02 21:34:08'),
(5, 'Mike Perez', 'CEO, bitches.', 25, '2015-06-02 21:34:30'),
(6, 'Isaac Perez', 'Haha, I win.', 25, '2015-06-02 21:34:49'),
(7, 'Derek Sanders', 'Im not flexing.', 25, '2015-06-02 21:35:28'),
(8, 'Jeff Chambers', 'BACKHAND --- thats in my baggy.', 25, '2015-06-02 21:36:39'),
(9, 'Robert Tindula', 'SEO-No', 25, '2015-06-02 21:38:06'),
(10, 'Tim Ronan', 'Ping Pong King Kong', 25, '2015-06-02 21:39:41');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE IF NOT EXISTS `ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rating` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `setz`
--

CREATE TABLE IF NOT EXISTS `setz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `matchz` int(11) NOT NULL,
  `team1` int(2) NOT NULL,
  `team2` int(2) NOT NULL,
  `start` varchar(20) NOT NULL,
  `finish` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=103 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
require 'Slim/Slim.php';
require_once 'db.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
$db = new dbHelper();

/**
 * Database Helper Function templates
 * select(table name, where clause as associative array)
 * insert(table name, data as associative array, mandatory column names as array)
 * update(table name, column names as associative array, where clause as associative array, required columns as array)
 * delete(table name, where clause as array)
*/

//======= GETS =======//
// Home oness
$app->get('/', 'getHome');
// Players
$app->get('/players', 'getPlayers');
$app->get('/players/:id', 'getPlayer');
// Games
$app->get('/games', 'getGames');
$app->get('/games/:id', 'getGame');
// Sets
$app->get('/sets', 'getSets');
$app->get('/sets/:id', 'getSet');
// Matches
$app->get('/matches', 'getMatchs');
$app->get('/matches/:id', 'getMatch');
// Ratings
$app->get('/ratings', 'getRatings');
$app->get('/ratings/:id', 'getRating');
//singlez
$app->get('/setzer', 'getLatestSet');
$app->get('/matcher', 'getLatestMatch');
$app->get('/active', 'getActive');

//======= POSTS =======//
//Players
$app->post('/players', 'postPlayer');
//games
$app->post('/games', 'postGame');
//Sets
$app->post('/sets', 'postSet');
//Match
$app->post('/matches', 'postMatch');
//Rating
$app->post('/ratings', 'postRating');
//Rating
$app->post('/active', 'postActive');

//======= PUTS =======//
//Players
$app->put('/players/update/:id', 'putPlayer');

//======= DELETES =======//
//Games
$app->delete('/games/delete/:id', 'deleteGame');

$app->run();

/*****************
 GET FUNCTIONS
*****************/
//  .../api/
function getHome() {
    echo "Welcome to ping pong!!!";
}
//  .../api/player
function getPlayers() {
    global $db;
    $rows = $db->select("players","id,name,nickname,ronan_rating,date_added",array());
    echoResponse(200, $rows);
}
function getPlayer($id) {
    global $db;
    $rows = $db->select("players","id,name,nickname,ronan_rating,date_added",array('id'=>$id));
    echoResponse(200, $rows);
}
//  .../api/game
function getGames() {
    global $db;
    $rows = $db->select("games","id,serving,setz,winner,time,body_shot",array());
    echoResponse(200, $rows);
}
function getGame($id) {
    global $db;
    $rows = $db->select("games","id,serving,setz,winner,time,body_shot",array('id'=>$id));
    echoResponse(200, $rows);
}
//  .../api/set
function getSets() {
    global $db;
    $rows = $db->select("setz","id,matchz,team1,team2,start,finish",array());
    echoResponse(200, $rows);
}
function getSet($id) {
    global $db;
    $rows = $db->select("setz","id,matchz,team1,team2,start,finish",array('id'=>$id));
    echoResponse(200, $rows);
}
//  .../api/match
function getMatchs() {
    global $db;
    $rows = $db->select("matchz","id,p1,p2,p3,p4,p1r,p2r,p3r,p4r,sets,games",array());
    echoResponse(200, $rows);
}
function getMatch($id) {
    global $db;
    $rows = $db->select("matchz","id,p1,p2,p3,p4,p1r,p2r,p3r,p4r,sets,games",array('id'=>$id));
    echoResponse(200, $rows);
}
//  .../api/ratings
function getRatings() {
    global $db;
    $rows = $db->select("ratings","id,player,date,rating",array());
    echoResponse(200, $rows);
}
function getRating($id) {
    global $db;
    $rows = $db->select("ratings","id,player,date,rating",array('id'=>$id));
    echoResponse(200, $rows);
}
// ....api/setzer
function getLatestSet(){
    global $db;
    $rows = $db->selectone("setz");
    echoResponse(200, $rows);
}
// ....api/matcher
function getLatestMatch(){
    global $db;
    $rows = $db->selectone("matchz");
    echoResponse(200, $rows);
}
// ....api/active
function getActive(){
    global $db;
    $rows = $db->selectone("active");
    echoResponse(200, $rows);
}

/*****************
 POST FUNCTIONS
*****************/
//  .../api/player
function postPlayer() {
    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());
    $mandatory = array('name');
    global $db;
    $rows = $db->insert("players", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Player successfully added.";
    echoResponse(200, $rows);
}
//  .../api/game
function postGame() {
    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());
    $mandatory = array('serving','setz','winner');
    global $db;
    $rows = $db->insert("games", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Game Recorded.";
    echoResponse(200, $rows);
}
//  .../api/set
function postSet() {
    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());
    $mandatory = array('matchz','team1','team2','start');
    global $db;
    $rows = $db->insert("setz", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Set Saved.";
    echoResponse(200, $rows);
}
//  .../api/match
function postMatch() {
    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());
    $mandatory = array('p1','p2','p1r','p2r','sets','games');
    global $db;
    $rows = $db->insert("matchz", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Match Saved.";
    echoResponse(200, $rows);
}
//  .../api/ratings
function postRating() {
    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());
    $mandatory = array('player','rating');
    global $db;
    $rows = $db->insert("ratings", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Rating Recorded / Updates.";
    echoResponse(200, $rows);
}
//  .../api/active
function postActive() {
    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());
    $mandatory = array('p1','p2','p3','p4','t1','t2','s1','s2','s3','s4','s5');
    global $db;
    $rows = $db->insert("active", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Player successfully added.";
    echoResponse(200, $rows);
}
/*****************
 PUT FUNCTIONS
*****************/
function putPlayer($id){ 
    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());
    $condition = array('id'=>$id);
    $mandatory = array();
    global $db;
    $rows = $db->update("players", $data, $condition, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Player updated successfully.";
    echoResponse(200, $rows);
}
/*****************
 DELETE FUNCTIONS
*****************/
function deleteGame($id) { 
    global $db;
    $rows = $db->delete("games", array('id'=>$id));
    if($rows["status"]=="success")
        $rows["message"] = "Update removed successfully.";
    echoResponse(200, $rows);
}

/***********************
    Utility Functions
***********************/
function echoResponse($status_code, $response) {
    global $app;
    $app->status($status_code);
    $app->contentType('application/json');
    echo json_encode($response,JSON_NUMERIC_CHECK);
}

?>
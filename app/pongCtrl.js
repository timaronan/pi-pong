app.controller('pongCtrl', function ($scope, $state, Data) {
    $scope.players = {};
    $scope.active = {};
    $scope.setsPlayed = [];
    $scope.statename = $state.current.name;
    $scope.playerTot, $scope.curMatch, $scope.curSetID;
    $scope.playerServing = {};
    $scope.playerServing.num = 1;
    $scope.playerServing.name = 'Player 1';
    $scope.bestOf = 3; 
    $scope.upTo = 15;
    $scope.serves = 0;
    $scope.gserves = 0;
    $scope.theset = 1;
    $scope.t1s = 0;
    $scope.t2s = 0;
    $scope.t1m = 0;
    $scope.t2m = 0;
    $scope.player1 = 'Player 1';
    $scope.player2 = 'Player 2';
    $scope.player3 = 'Player 3';
    $scope.player4 = 'Player 4';
    $scope.playerz = false;
    $scope.iOS = ['iPad', 'iPhone', 'iPod'].indexOf(navigator.platform) >= 0;


    Data.get('players').then(function(data){
        $scope.players = data.data;
    });
    Data.get('active').then(function(data){
        $scope.active = data.data[0].p1;
    });

    $scope.formInput = function( idn , ran , num , pnm ) {
        this.newMatch['p'+num] = {};
        this.newMatch['p'+num].idn = idn;
        this.newMatch['p'+num].ran = ran;
        $scope['player'+num] = pnm;
        if( $scope.player1 == $scope.player2 ||  $scope.player1 == $scope.player3 || $scope.player1 == $scope.player4 
            || $scope.player2 == $scope.player3 || $scope.player2 == $scope.player4 || $scope.player3 == $scope.player4){
            $scope.playerz = false;
        }else{
            $scope.playerz = true;
        }
    }

    $scope.boM = function( val ) {
        this.newMatch.sets = val;
        $scope.bestOf = val;
    }

    $scope.utM = function( val ) {
        this.newMatch.games = val;
        $scope.upTo = val;
    }

    $scope.servM = function( val ) {
        $scope.playerServing.num = val;
        $scope.playerServing.name = $scope['player'+val];
    }

    $scope.btnInit = function(){
        if($state.current.name === 'ingame'){
            $state.go('.initiate');
        }else{
            $state.go('^.initiate');
        }
        resetAllPoints();
    }

    $scope.btnLive = function(){
        $state.go('live');
    }

    $scope.startMatch = function (newmatch) {
        $scope.playerTot = 2;
        $scope.setsPlayed.push({'team1' : 0, 'team2' : 0});

        $state.go('^.action');

        newmatch.p1r = newmatch.p1.ran;
        newmatch.p1 = newmatch.p1.idn;
        newmatch.p2r = newmatch.p2.ran;
        newmatch.p2 = newmatch.p2.idn;

        if(newmatch.p3){
            newmatch.p3r = newmatch.p3.ran;
            newmatch.p3 = newmatch.p3.idn;
            $scope.playerTot += 1;
        }

        if(newmatch.p4){
            newmatch.p4r = newmatch.p4.ran;
            newmatch.p4 = newmatch.p4.idn;
            $scope.playerTot += 1;
        }
        
        $scope.playerServing.name = $scope['player'+$scope.playerServing.num];

        Data.post('matches', newmatch).then(function (result) {
            if(result.status != 'error'){
                Data.get('matcher').then(function (mtch) {
                    $scope.curMatch = mtch.data[0];
                });
                Data.get('setzer').then(function (stz) {
                    if(stz.data.length > 0){
                        $scope.curSetID = stz.data[0].id + 1;
                    }else{
                        var gamez = {};
                        gamez.matchz = 0;
                        gamez.team1 = 0;
                        gamez.team2 = 0;
                        gamez.start = 0;
                        Data.post('sets', gamez).then(function (bbb) {
                            if(result.status != 'error'){
                                Data.get('setzer').then(function (ccc) {
                                    if(ccc.data.length > 0){
                                        $scope.curSetID = ccc.data[0].id + 1;
                                    }
                                });
                            }else{
                                alert(result.message);
                            }
                        });
                    }
                });
            }else{
                alert(result.message);
            }
        });

        var activematch = {};
        activematch.p1 = $scope.player1;
        activematch.p2 = $scope.player2;
        activematch.p3 = $scope.player3;
        activematch.p4 = $scope.player4;
        activematch.t1 = 0;
        activematch.t2 = 0;
        activematch.s1 = 0;
        if( $scope.bestOf == 1 ){
            activematch.s2 = 'none';
            activematch.s3 = 'none';
            activematch.s4 = 'none';
            activematch.s5 = 'none';
        }
        if( $scope.bestOf == 3 ){
            activematch.s2 = 0;
            activematch.s3 = 0;

            activematch.s4 = 'none';
            activematch.s5 = 'none';
        }
        if( $scope.bestOf == 5 ){
            activematch.s2 = 0;
            activematch.s3 = 0;
            activematch.s4 = 0;
            activematch.s5 = 0;
        }

        Data.post('active', activematch);

    };

    $scope.scorePoint = function (pointz) {

        $scope.playerServing.num = parseInt($scope.playerServing.num);
        pointz.serving = $scope.playerServing.num;
        pointz.setz = parseInt($scope.curSetID);
        $scope.serves += 1;
        $scope.gserves += 1;

        Data.post('games', pointz).then(function (result) {
            if(result.status != 'error'){
                var x = angular.copy(pointz);
                x.save = 'insert';
                x.id = result.data;
            }else{
                alert(result.message);
            }
        });

        if( pointz.winner == 1 ){
            $scope.t1s += 1;
            $scope.setsPlayed[$scope.theset - 1].team1 += 1;
            if(pointz.body_shot == 1){
                $scope.t1s += 1;
                $scope.setsPlayed[$scope.theset - 1].team1 += 1;
            }
            if( ! $scope.iOS ){
                scoreImg(1);
            }
        }else if( pointz.winner == 2 ){
            $scope.t2s += 1;
            $scope.setsPlayed[$scope.theset - 1].team2 += 1;
            if(pointz.body_shot == 1){
                $scope.t2s += 1;
                $scope.setsPlayed[$scope.theset - 1].team2 += 1;
            }
            if( ! $scope.iOS ){
                scoreImg(2);
            }
        }

        if($scope.playerTot === 4){
            if( $scope.gserves % 5 === 0 ){
                if( parseInt($scope.playerServing.num) === parseInt($scope.playerTot) ){
                    $scope.playerServing.num = 1;
                }else{
                    $scope.playerServing.num += 1;
                }
                $scope.playerServing.name = $scope['player'+$scope.playerServing.num];
            }           
        }else{
            if( $scope.gserves % 3 === 0 ){
                if( parseInt($scope.playerServing.num) === parseInt($scope.playerTot) ){
                    $scope.playerServing.num = 1;
                }else{
                    $scope.playerServing.num += 1;
                }
                $scope.playerServing.name = $scope['player'+$scope.playerServing.num];
            }            
        }

        beLive();

        //TEam 1 wins
        if( $scope.t1s >= $scope.upTo && $scope.t2s < ( $scope.t1s - 1 ) ){
            rscoreImg(1);
            rscoreImg(2);
            $scope.t1m += 1;
            $scope.theset += 1;
            $scope.gserves = 0;

            var gamez = {};
            gamez.matchz = $scope.curMatch.id;
            gamez.team1 = $scope.t1s;
            gamez.team2 = $scope.t2s;
            gamez.start = $scope.curMatch.initiated;
            Data.post('sets', gamez).then(function (result) {
                if(result.status != 'error'){
                    $scope.curSetID += 1;
                }else{
                    alert(result.message);
                }
            });

            $scope.playerServing.num = 1;
            $scope.t1s = 0;
            $scope.t2s = 0;

            if( $scope.t1m == Math.ceil( $scope.bestOf / 2 ) ){
                //Team wins match
                $scope.winner = {};
                $scope.winner.p1 = $scope.player1;
                if($scope.player3 != 'Player 3'){
                    $scope.winner.p2 = ' & ' + $scope.player3 + ' Win!!!';
                }else{
                    $scope.winner.p2 = ' Wins!!!';
                }
                $state.go('^.winner');
                beLive(true);
            }else{
                $scope.setsPlayed.push({'team1' : 0, 'team2' : 0});
            }
        //TEam 2 wins
        }else if( $scope.t2s >= $scope.upTo && $scope.t1s < ( $scope.t2s - 1 ) ){
            rscoreImg(1);
            rscoreImg(2);
            $scope.t2m += 1;
            $scope.theset += 1;
            $scope.gserves = 0;

            var gamez = {};
            gamez.matchz = $scope.curMatch.id;
            gamez.team1 = $scope.t1s;
            gamez.team2 = $scope.t2s;
            gamez.start = $scope.curMatch.initiated;
            Data.post('sets', gamez).then(function (result) {
                if(result.status != 'error'){
                    $scope.curSetID += 1;
                }else{
                    alert(result.message);
                }
            });

            $scope.playerServing.num = 2;
            $scope.t1s = 0;
            $scope.t2s = 0;

            if( $scope.t2m == Math.ceil( $scope.bestOf / 2 ) ){
                //Team wins match
                $scope.winner = {};
                $scope.winner.p1 = $scope.player2;
                if($scope.player4 != 'Player 4'){
                    $scope.winner.p2 = ' & ' + $scope.player4 + ' Win!!!';
                }else{
                    $scope.winner.p2 = ' Wins!!!';
                }
                $state.go('^.winner');
                beLive(true);
            }else{
                $scope.setsPlayed.push({'team1' : 0, 'team2' : 0});
            }
        }


        function beLive(reseter){
            var activematch = {};
            if(reseter){
                activematch.p1 = 'none';
                activematch.p2 = 'none';
                activematch.p3 = 'none';
                activematch.p4 = 'none';
                activematch.t1 = 0;
                activematch.t2 = 0;
                activematch.s1 = 'none';
                activematch.s2 = 'none';
                activematch.s3 = 'none';
                activematch.s4 = 'none';
                activematch.s5 = 'none';
            }else{
                activematch.p1 = $scope.player1;
                activematch.p2 = $scope.player2;
                activematch.p3 = $scope.player3;
                activematch.p4 = $scope.player4;
                activematch.t1 = $scope.t1s;
                activematch.t2 = $scope.t2s;
                activematch.s1 = $scope.setsPlayed[0].team1 + " - " + $scope.setsPlayed[0].team2;
                if( $scope.theset == 1){
                    activematch.s2 = 'none';
                    activematch.s3 = 'none';
                    activematch.s4 = 'none';
                    activematch.s5 = 'none';
                }
                if( $scope.theset == 2){
                    activematch.s2 = $scope.setsPlayed[1].team1 + " - " + $scope.setsPlayed[1].team2;
                    activematch.s3 = 'none';
                    activematch.s4 = 'none';
                    activematch.s5 = 'none';
                }
                if( $scope.theset == 3){
                    activematch.s2 = $scope.setsPlayed[1].team1 + " - " + $scope.setsPlayed[1].team2;
                    activematch.s3 = $scope.setsPlayed[2].team1 + " - " + $scope.setsPlayed[2].team2;
                    activematch.s4 = 'none';
                    activematch.s5 = 'none';
                }        
                if( $scope.theset == 4){
                    activematch.s2 = $scope.setsPlayed[1].team1 + " - " + $scope.setsPlayed[1].team2;
                    activematch.s3 = $scope.setsPlayed[2].team1 + " - " + $scope.setsPlayed[2].team2;
                    activematch.s4 = $scope.setsPlayed[3].team1 + " - " + $scope.setsPlayed[3].team2;
                    activematch.s5 = 'none';
                }  
                if( $scope.theset == 5){
                    activematch.s2 = $scope.setsPlayed[1].team1 + " - " + $scope.setsPlayed[1].team2;
                    activematch.s3 = $scope.setsPlayed[2].team1 + " - " + $scope.setsPlayed[2].team2;
                    activematch.s4 = $scope.setsPlayed[3].team1 + " - " + $scope.setsPlayed[3].team2;
                    activematch.s5 = $scope.setsPlayed[4].team1 + " - " + $scope.setsPlayed[4].team2;
                }
            }

            updateStats();

            function updateLive(){
                console.log('live');
                Data.delete('active/delete').then(function(data){
                    updateStats();
                });
            }
            function updateStats(){
                Data.post('active', activematch).then(function(data){
                    $scope.liveStats = data.data;
                });        
            }            
        }      

    };

    $scope.endNow = function () {

        resetAllPoints();
        $state.go('^');

    };

    function scoreImg(pn){
        if( $scope.iOS ){
            d3.select('.current-score .team'+pn+'.score .second').select('svg').attr('viewBox', '0 '+ (50 * ($scope['t'+pn+'s']%10)) +' 25 50');
            if($scope['t'+pn+'s']%10 == 0){
                d3.select('.current-score .team'+pn+'.score .first').select('svg').attr('viewBox', '0 '+ (50 * Math.floor(($scope['t'+pn+'s']/10))) +' 25 50');
            }            
        }else{
            d3.select('.current-score .team'+pn+'.score .second').select('svg').transition().attr('viewBox', '0 '+ (50 * ($scope['t'+pn+'s']%10)) +' 25 50');
            if($scope['t'+pn+'s']%10 == 0){
                d3.select('.current-score .team'+pn+'.score .first').select('svg').transition().attr('viewBox', '0 '+ (50 * Math.floor(($scope['t'+pn+'s']/10))) +' 25 50');
            }
        }
    }

    function rscoreImg(pn){

        d3.select('.current-score .team'+pn+'.score .second').select('svg').transition().attr('viewBox', '0 0 25 50');
        d3.select('.current-score .team'+pn+'.score .first').select('svg').transition().attr('viewBox', '0 0 25 50');

    }

    function resetAllPoints(){
        $scope.setsPlayed = [];
        $scope.statename = $state.current.name;
        $scope.playerTot = 2;
        $scope.curMatch = {};
        $scope.curSetID = 0;
        $scope.playerServing.num = 1;
        $scope.bestOf = 3; 
        $scope.upTo = 15;
        $scope.serves = 0;
        $scope.theset = 1;
        $scope.t1s = 0;
        $scope.t2s = 0;
        $scope.t1m = 0;
        $scope.t2m = 0;
        $scope.player1 = 'Player 1';
        $scope.player2 = 'Player 2';
        $scope.player3 = 'Player 3';
        $scope.player4 = 'Player 4';
    }

});

app.controller('liveCtrl', function ($scope, $state, Data) {

    $scope.liveStats = {};
    $scope.setsPlayed = [];

    updateLive();

    function updateLive(){
        Data.get('active').then(function(data){

            console.log(data, $scope.liveStats, $scope.setsPlayed);

            var match;
            $scope.liveStats = data.data[0];
            $scope.setsPlayed = [];

            if( $scope.liveStats.s1 != 'none' ){
                match = $scope.liveStats.s1.split(" - ");
                $scope.setsPlayed.push(match);
            }

            if( $scope.liveStats.s2 != 'none' ){
                match = $scope.liveStats.s2.split(" - ");
                $scope.setsPlayed.push(match);                
            }

            if( $scope.liveStats.s3 != 'none' ){
                match = $scope.liveStats.s3.split(" - ");
                $scope.setsPlayed.push(match);                
            }

            if( $scope.liveStats.s4 != 'none' ){
                match = $scope.liveStats.s4.split(" - ");
                $scope.setsPlayed.push(match);                
            }

            if( $scope.liveStats.s5 != 'none' ){
                match = $scope.liveStats.s5.split(" - ");
                $scope.setsPlayed.push(match);                
            }

        });  

        setTimeout(function(){
            updateLive();
        }, 1000);

    }

});
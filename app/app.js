var app = angular.module('myApp', ['ui.router', 'ngAnimate', 'angular-preload-image']);

app.config(['$locationProvider', function($locationProvider){
    $locationProvider.html5Mode(true).hashPrefix('!');
}]);

app.config(
  function($stateProvider, $urlRouterProvider) {
    $stateProvider.
    state('ingame', {
      url: '/',
      title: 'Welcome to Highrank Ping Pong',
      templateUrl: 'partials/ingame.html',
      controller: 'pongCtrl'
    })
    .state('ingame.initiate', {
      // url: '^/initiate',
      // title: 'Initiate Pong Game',
      templateUrl: 'partials/ingame-initiate.html',
    })
    .state('ingame.action', {
      // url: '^/action',
      // title: 'Live Pong Game',
      templateUrl: 'partials/ingame-action.html',
    })
    .state('ingame.winner', {
      // url: '^/action',
      // title: 'Live Pong Game',
      templateUrl: 'partials/ingame-winner.html',
    })
    .state('live', {
      url: '/live',
      title: 'See live results',
      templateUrl: 'partials/live.html',
      controller: 'liveCtrl'
    });

    $urlRouterProvider.otherwise('/');
});

app.filter('dateToISO', function() {
  return function(input) {
    input = new Date(input).toISOString();
    return input;
  };
});

app.run(function ($state,$rootScope) {
  $rootScope.$state = $state;
});

app.run(function() {
  FastClick.attach(document.body);
});
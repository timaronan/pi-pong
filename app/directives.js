app.directive('voronoi', function(){

    return {
        restrict: 'A',      
        link: function (scope, iElement, iAttrs) {
            var iOS = ['iPad', 'iPhone', 'iPod'].indexOf(navigator.platform) >= 0;
            var width = d3.select(iElement[0].parentNode).style('width').replace('px', ''),
                height = d3.select(iElement[0].parentNode).style('height').replace('px', ''),
                offw = width * 0.1,
                offh = height * 0.1,
                iterats = 0;
            pos1 = {};
            pos1.h = true;
            pos1.w = true;
            pos2 = {};
            pos2.h = false;
            pos2.w = false;
            pos3 = {};
            pos3.h = true;
            pos3.w = false;
            pos4 = {};
            pos4.h = false;
            pos4.w = true;            

            var vertices = d3.range(100).map(function(d) {
              return [Math.random() * width, Math.random() * height];
            });

            var svg = d3.select(iElement[0]).append("svg")
                .attr("width", "120%")
                .attr("height", "120%")
                .attr("viewBox", offw+" "+offh+" "+(parseInt(width) - offw)+" "+(parseInt(height) - offh))
                .attr("preserveAspectRatio", "xMidYMid meet");

            var path = svg.append("g").selectAll("path");

            vertices[0] = [offw,offh];        
            vertices[1] = [width,height]; 
            vertices[2] = [offw,height];        
            vertices[3] = [width,offh]; 

            redraw();

            function redraw() {
                iterats += 1;

                if( iAttrs.dynamic ){
                    polymove(vertices[0], pos1);
                    polymove(vertices[1], pos2);
                    polymove(vertices[2], pos3);
                    polymove(vertices[3], pos4);
                }

                if (iterats % 20 === 0 && iAttrs.subtle) {
                    polymove(vertices[ Math.floor( Math.random() * vertices.length ) ], pos1);
                    polymove(vertices[ Math.floor( Math.random() * vertices.length ) ], pos2);
                    polymove(vertices[ Math.floor( Math.random() * vertices.length ) ], pos3);
                    polymove(vertices[ Math.floor( Math.random() * vertices.length ) ], pos4);                        
                }; 

                path = path.data(d3.geom.delaunay(vertices).map(function(d) { return "M" + d.join("L") + "Z"; }), String);
                path.transition().attr("d", String);
                path.exit().remove();
                path.enter().append("path").attr("class", function(d, i) { return "q" + (i % 7); }).attr("d", String);

                if( !iOS ){
                    if( iAttrs.redrawing ){
                        setTimeout(function(){
                            redraw();
                        }, (Math.random() * 75));
                    }
                }               
            }


            function polymove(el, pz){

                if( el[0] > (width - offw) || el[1] > (height - offh) ){
                    if( el[0] > (width - offw) ){
                        el[0] = (width - offw);
                        pz.w = false;
                    }
                    if( el[1] > (height - offh) ){
                        el[1] = (height - offh);
                        pz.h = false;
                    }
                }else if( el[0] < (offw) || el[1] < (offh) ){
                    if( el[0] < (offw) ){
                        el[0] = offw;
                        pz.w = true;
                    }
                    if( el[1] < (offh) ){
                        el[1] = offh;
                        pz.h = true;
                    }
                }

                if(pz.w){
                    el[0] += (Math.random() * 2);
                }else{
                    el[0] -= (Math.random() * 2);
                }
            
                if(pz.h){
                    el[1] += (Math.random() * 2);
                }else{
                    el[1] -= (Math.random() * 2);                
                }
            }

            function polygon(d) {
              return "M" + d.join("L") + "Z";
            }
        }
    };

});

app.directive('animatepoly', function(){
    return function(scope, element) {
        var iOS = ['iPad', 'iPhone', 'iPod'].indexOf(navigator.platform) >= 0;
        if( !iOS ){

            var colors = ["#7D0A44", "#9B0B4D", "#DC177A", "#D01160", "#6B083F", "#C20F5C"];
            var delay = 3000;
            rotateColor();
            d3.select(element[0]).select("#numbers").selectAll('polygon, path')
                .on('mouseenter', function(){
                    var vlz = Math.floor( colors.length * Math.random() );
                    d3.select(this).transition().duration(50).attr('fill', colors[ vlz ]);
                });
            function rotateColor(){
                d3.select(element[0]).select("#numbers").selectAll('polygon, path')
                    .transition().duration(delay).attr('fill', function(){
                        var vlz = Math.floor( colors.length * Math.random() );
                        return colors[ vlz ];
                    });
                setTimeout(function(){
                    rotateColor();
                }, delay);
            }

        }
    } 
});

app.directive('selecttag', function(){
    return function(scope, element) {
        var contr = d3.select(element[0]);
        var btn = contr.select('h4');

        btn.on('click', function(){
            contr.classed('open', true);
        });

    }
});

app.directive('liz', function () {
    return {
        restrict: 'A',
        link: function (scope, iElement, iAttrs) {
            var contr = d3.select(iElement[0].parentNode.parentNode);
            d3.select(iElement[0]).on('click', function(){
                contr.classed('open', false);
            });
        }
    };
});